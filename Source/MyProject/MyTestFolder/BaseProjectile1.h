// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseProjectile1.generated.h"

class UProjectileMovementComponent;
class UStaticMeshComponent;
class UAudioComponent;
class UParticleSystemComponent;

DECLARE_DYNAMIC_DELEGATE_OneParam(FHitResultDelegate, FHitResult, Hit);

UCLASS()
class MYPROJECT_API ABaseProjectile1 : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABaseProjectile1();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UProjectileMovementComponent* MovementComp;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* Bullet;
	UPROPERTY(EditDefaultsOnly, Category = "Recover")
		FTimerHandle RecoverTime_TH;
	
	


public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY()
		FHitResultDelegate OnHitFire;

	virtual void NotifyHit(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit) override;

};
