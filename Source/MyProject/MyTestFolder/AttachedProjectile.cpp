// Fill out your copyright notice in the Description page of Project Settings.


#include "AttachedProjectile.h"
#include <TimerManager.h>
#include <GameFramework/ProjectileMovementComponent.h>





void AAttachedProjectile::NotifyHit(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit)
{
	ExpHit = Hit;

	
	GetWorld()->GetTimerManager().SetTimer(ExplosionTime_TH, this, &AAttachedProjectile::ExplosionPlay, 1, false, 3);

}

void AAttachedProjectile::ExplosionPlay()
{
	if (OnHitFire.IsBound())
	{
		OnHitFire.Execute(ExpHit);
	}
	Destroy();
}
