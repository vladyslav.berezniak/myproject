// Fill out your copyright notice in the Description page of Project Settings.


#include "VitalityComponent.h"
#include <UnrealString.h>
#include "TimerManager.h"
#include "MyGameInstance.h"
#include "BasePickup.h"
#include "Kismet\KismetMathLibrary.h"
#include <UnrealMathUtility.h>


// Sets default values for this component's properties
UVitalityComponent::UVitalityComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	CurrentHealth = MaxHealth;

	// ...
}



void UVitalityComponent::DamageHandle(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if (bCanTakeDamage)
	{
		if (IsAlive())
		{
			UE_LOG(LogTemp, Warning, TEXT("%s was damaged for %s"), *DamagedActor->GetName(), *FString::SanitizeFloat(Damage));

			CurrentHealth -= Damage;

			OnHealthChanged.Broadcast();
		}
		if (!IsAlive())
		{
			//Death
			OnOwnerDied.Broadcast();
			bCanTakeDamage = false;
			UMyGameInstance* MyInstance = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
			MyInstance->AddScore(Score);
			UE_LOG(LogTemp, Warning, TEXT("Owner died"));
			if (bSpawnOnDeath)
			{
				FVector LocationToSpawn = GetOwner()->GetActorLocation();
				FRotator RotationToSpawn = GetOwner()->GetActorRotation();
				SpawnPickup(LocationToSpawn, RotationToSpawn);
			}
		}
	}
}

bool UVitalityComponent::IsAlive()
{
	return CurrentHealth > 0;
}


void UVitalityComponent::RecoverDamage()
{
	if (CurrentHealth < MaxHealth && CurrentHealth > 0)
	{
		CurrentHealth += RecoverStep;
		GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Emerald, TEXT("Current Health is ") + FString::FromInt(CurrentHealth));

		OnHealthChanged.Broadcast();
	}
	else
	{
		StopRecover();
	}
}

bool UVitalityComponent::ShouldSpawn(float Chance)
{
	return (UKismetMathLibrary::RandomBoolWithWeight(Chance) == 1 ? true : false);
}

void UVitalityComponent::StartRecover()
{
	GetOwner()->GetWorldTimerManager().SetTimer(RecoverTime_TH, this, &UVitalityComponent::RecoverDamage, RecoverStepTime, true, RecoverDelay);
}

void UVitalityComponent::StopRecover()
{
	GetOwner()->GetWorldTimerManager().ClearTimer(RecoverTime_TH);
}

// Called when the game starts
void UVitalityComponent::BeginPlay()
{
	Super::BeginPlay();
	CurrentHealth = MaxHealth;
	if (GetOwner())
	{
		GetOwner()->OnTakeAnyDamage.AddDynamic(this, &UVitalityComponent::DamageHandle);
	}
	OnHealthChanged.Broadcast();
	// ...

}



// Called every frame
void UVitalityComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UVitalityComponent::AddHealth(float HealthToAdd)
{
	if (CurrentHealth > 0 && CurrentHealth != MaxHealth)
	{
		float NewHealth = CurrentHealth + HealthToAdd;
		if (NewHealth < MaxHealth)
		{
			CurrentHealth = NewHealth;
		}
		else
		{
			CurrentHealth = MaxHealth;
		}
		OnHealthChanged.Broadcast();
	}
}

void UVitalityComponent::SpawnPickup(FVector Loc, FRotator Rot)
{
	FActorSpawnParameters SpawnParams;
	if (SpawnMap.Num() > 0)
	{
		for (auto& Elem : SpawnMap)
		{
			Loc.X += FMath::RandRange(50.0f, 50.0f);
			Loc.Y += FMath::RandRange(50.0f, 50.0f);
			if (ShouldSpawn(Elem.Value))
			{
				ABasePickup* SpawnedPickupRef = GetWorld()->SpawnActor<ABasePickup>(Elem.Key, Loc, Rot, SpawnParams);
			}
		}
	}
}
