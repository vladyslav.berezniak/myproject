// Fill out your copyright notice in the Description page of Project Settings.


#include "MyCharacter.h"
#include <Components/InputComponent.h>
#include <GameFramework/SpringArmComponent.h>
#include <Camera/CameraComponent.h>
#include <ConstructorHelpers.h>
#include <GameFramework/Actor.h>
#include <GameFramework/CharacterMovementComponent.h>
#include <GameFramework/Character.h>
#include <RotationMatrix.h>
#include <Components/SkeletalMeshComponent.h>
#include <Animation/AnimMontage.h>
#include <Runtime\Engine\Public\SkeletalMeshMerge.h>
#include <Runtime\Engine\Public\SkeletalMeshMerge.h>
#include <Components/SceneComponent.h>
#include "BaseWeapon.h"
#include <Engine/EngineTypes.h>
#include "VitalityComponent.h"


// Sets default values
AMyCharacter::AMyCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	VitalityComp = CreateDefaultSubobject<UVitalityComponent>(TEXT("VitalityComponent"));

	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArmComponent->SetupAttachment(RootComponent);

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetupAttachment(SpringArmComponent);

	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SkelMesh(TEXT("SkeletalMesh'/Game/AnimStarterPack/UE4_Mannequin/Mesh/SK_Mannequin.SK_Mannequin'"));

	if (SkelMesh.Succeeded())
	{
		GetMesh()->SetSkeletalMesh(SkelMesh.Object);
	}

	GetMesh()->AddLocalTransform(FTransform(FRotator(0, -90, 0).Quaternion()));

	GetCharacterMovement()->MaxWalkSpeed = DefaultWalkSpeed;
	
	SpringArmComponent->TargetArmLength = 1000.0f;
	SpringArmComponent->AddLocalTransform(FTransform(FRotator(-90, 0, 0).Quaternion(), FVector(0, 0, 0)));

	CameraMultiplier = 10.0f;
	MaxCameraHeight = 1200.0f;
	MinCameraHeight = 800.0f;

	bUseControllerRotationYaw = false;

	SpringArmComponent->bInheritPitch = false;
	SpringArmComponent->bInheritYaw = false;
	SpringArmComponent->bInheritRoll = false;

	VitalityComp->OnOwnerDied.AddDynamic(this, &AMyCharacter::CharacterDied);
}

// Called when the game starts or when spawned
void AMyCharacter::BeginPlay()
{
	Super::BeginPlay();

	CurrentWeapon = SpawnWeapon();
	AttachWeapon(WeaponSocket);
}


void AMyCharacter::MoveForward(float Scale)
{
	if (!bIsSprinting)
	{
		AddMovementInput(FVector(1, 0, 0), Scale);
	}
	else
	{
		AddMovementInput(GetActorForwardVector(), 1.0);
	}
}

void AMyCharacter::MoveRight(float Scale)
{
	if (!bIsSprinting)
	{
		AddMovementInput(FVector(0, 1, 0), Scale);
	}
	else
	{
		AddMovementInput(GetActorForwardVector(), 1.0);
	}
}

void AMyCharacter::CameraZoom(float Scale)
{
	if ((Controller != NULL) && (Scale != 0.0f))
	{
		float NewCameraHeigh = SpringArmComponent->TargetArmLength + Scale * CameraMultiplier;

		if (NewCameraHeigh <= MaxCameraHeight && NewCameraHeigh >= MinCameraHeight)
		{
			SpringArmComponent->TargetArmLength = NewCameraHeigh;
		}
	}
}


void AMyCharacter::Sprint()
{
	GetCharacterMovement()->MaxWalkSpeed = SprintSpeed;
	bIsSprinting = true;
	bCanFire = false;
}

void AMyCharacter::StopSprint()
{
	GetCharacterMovement()->MaxWalkSpeed = DefaultWalkSpeed;
	bIsSprinting = false;
	bCanFire = true;
}

void AMyCharacter::StartWalk()
{
	GetCharacterMovement()->MaxWalkSpeed = GetCharacterMovement()->MaxWalkSpeedCrouched ;
	bIsWalking = true;
}

void AMyCharacter::StopWalk()
{
	GetCharacterMovement()->MaxWalkSpeed = DefaultWalkSpeed;
	bIsWalking = false;
}

void AMyCharacter::OnJump()
{
	Jump();
}



void AMyCharacter::Fire()
{
}

void AMyCharacter::Reload()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Reload();
	}
}

void AMyCharacter::EquipWeapon()
{
	if (EquipCurrentWeapon)
	{
		GetMesh()->GetAnimInstance()->Montage_Play(EquipCurrentWeapon);
	}
}

void AMyCharacter::UnEquipWeapon()
{
	if (UnEquipCurrentWeapon)
	{
		GetMesh()->GetAnimInstance()->Montage_Play(UnEquipCurrentWeapon);
	}
}

ABaseWeapon* AMyCharacter::SpawnWeapon()
{
	if (WeaponToSpawn_Class)
	{
		FTransform TmpTransform = FTransform::Identity;
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.Owner = this;

		UE_LOG(LogTemp, Warning, TEXT("Trying to spawn wepon"));
		return	Cast<ABaseWeapon>(GetWorld()->SpawnActor(WeaponToSpawn_Class, &TmpTransform, SpawnParameters));
	}
	return nullptr;
}

void AMyCharacter::AttachWeapon(FName SocketName)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->GetSkeletalMeshComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		CurrentWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, SocketName);
		OnWeaponChanged.Broadcast(CurrentWeapon);
	}
}

void AMyCharacter::AttachWeapon()
{
	/*if (WeaponToSpawn_Class)
	{
		CurrentWeapon->WeaponEnumType == EWeaponTypeEnum::DE_Primary;
	}*/
}

void AMyCharacter::StartFire()
{
	if (CurrentWeapon && bCanFire)
	{
		CurrentWeapon->StartFire();
	}
}

void AMyCharacter::StopFire()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StopFire();
	}
}

void AMyCharacter::Drop()
{
	CurrentWeapon->Destroy();
}

void AMyCharacter::SpeedBoost(float NewSpeed)
{
	GetCharacterMovement()->MaxWalkSpeed = NewSpeed;
}


// Called every frame
void AMyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	RotateToMouse();

}

void AMyCharacter::RotateToMouse()
{
	FHitResult Hit;

	if (!VitalityComp->IsAlive())
	{
		return;
	}

	if (GetWorld()->GetFirstPlayerController()->GetHitResultUnderCursor(ECC_Visibility, false, Hit))
	{
		float RotationYaw = FRotationMatrix::MakeFromX(Hit.ImpactPoint - GetActorLocation()).Rotator().Yaw;
		SetActorRotation(FRotator(0, RotationYaw, 0));
	}
}

void AMyCharacter::StartRoll()
{
	if (!bIsWalking && !bIsSprinting && !bIsRolling)
	{
		bIsRolling = true;
		bCanFire = false;
		VitalityComp->bCanTakeDamage = false;
		GetWorldTimerManager().SetTimer(RollingTime_TH, this, &AMyCharacter::StopRoll, 0.7, false);

	}
}

void AMyCharacter::StopRoll()
{
	bIsRolling = false;
	bCanFire = true;
	VitalityComp->bCanTakeDamage = true;
	GetWorldTimerManager().ClearTimer(RollingTime_TH);
}

void AMyCharacter::CharacterDied()
{
	GetCharacterMovement()->DisableMovement();
	bUseControllerRotationYaw = false;
	StopFire();

	CurrentWeapon = nullptr;
}

// Called to bind functionality to input
void AMyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AMyCharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AMyCharacter::MoveRight);
	PlayerInputComponent->BindAction(TEXT("Sprint"), IE_Pressed, this, &AMyCharacter::Sprint);
	PlayerInputComponent->BindAction(TEXT("Sprint"), IE_Released, this, &AMyCharacter::StopSprint);
	PlayerInputComponent->BindAction(TEXT("Walk"), IE_Pressed, this, &AMyCharacter::StartWalk);
	PlayerInputComponent->BindAction(TEXT("Walk"), IE_Released, this, &AMyCharacter::StopWalk);
	PlayerInputComponent->BindAxis(TEXT("CameraZoom"), this, &AMyCharacter::CameraZoom);
	PlayerInputComponent->BindAction(TEXT("Jump"), IE_Pressed, this, &AMyCharacter::StartRoll);
	PlayerInputComponent->BindAction(TEXT("Reload"), IE_Pressed, this, &AMyCharacter::Reload);
	PlayerInputComponent->BindAction(TEXT("Weapon_Primary"), IE_Pressed, this, &AMyCharacter::EquipWeapon);
	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Pressed, this, &AMyCharacter::StartFire);
	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Released, this, &AMyCharacter::StopFire);
}

