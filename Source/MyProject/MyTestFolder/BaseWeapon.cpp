// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseWeapon.h"
#include "Animation/SkeletalMeshActor.h"
#include <MessageDialog.h>
#include <Components/SkeletalMeshComponent.h>
#include <GameFramework/Character.h>
#include <Animation/AnimMontage.h>
#include <GameFramework/Actor.h>
#include "TimerManager.h"
#include "ImpactEffectComponent.h"
#include <UnrealString.h>
#include <Kismet/GameplayStatics.h>


ABaseWeapon::ABaseWeapon()
{
	GetSkeletalMeshComponent()->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	GetSkeletalMeshComponent()->SetMobility(EComponentMobility::Movable);

	ImpactEffectComp = CreateDefaultSubobject<UImpactEffectComponent>(TEXT("ImpactEffectComp"));
}

void ABaseWeapon::StartFire()
{
	if (bIsChargable)
	{
		if (ChargeEmitter)
		{
			class UParticleSystemComponent* ChargeEmitterComp = UGameplayStatics::SpawnEmitterAttached(ChargeEmitter, RootComponent, MuzzleSocketName, FVector::ZeroVector, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, true);
			
		}
	}
	GetWorldTimerManager().SetTimer(AutoFite_TH, this, &ABaseWeapon::Fire, 60 / FireRate, bAutoFire, ChargeTime);
}

void ABaseWeapon::StopFire()
{
	GetWorldTimerManager().ClearTimer(AutoFite_TH);
}


void ABaseWeapon::Fire()
{
	if (CurrentClip > 0)
	{
		bool bMontageIsPlaying = WeaponOwner->GetMesh()->GetAnimInstance()->Montage_IsPlaying(ReloadMontage);
		if (!bMontageIsPlaying)
		{
			MontageAnimPlay(WeaponOwner, FireMontage, FireAnim);
			CurrentClip--;
			OnAmmoChanged.Broadcast();
		}
	}
	else
	{
		Reload();
	}
}


void ABaseWeapon::Reload()
{
	if (TotalAmmo == 0)
	{
		GEngine->AddOnScreenDebugMessage(0, 2.0f, FColor::Red, TEXT("Out of Ammo"));
		return;
	}
	else if (CurrentClip == ClipSize)
	{
		GEngine->AddOnScreenDebugMessage(0, 2.0f, FColor::Red, TEXT("Full clip"));
		return;
	}
	else if (TotalAmmo + CurrentClip >= ClipSize)
	{
		TotalAmmo += CurrentClip - ClipSize;
		CurrentClip = ClipSize;
	}
	else
	{
		CurrentClip += TotalAmmo;
		TotalAmmo = 0;
	}
	MontageAnimPlay(WeaponOwner, ReloadMontage, ReloadAnim);
	OnAmmoChanged.Broadcast();
}


FTransform ABaseWeapon::GetMuzzleTransform()
{
	if (MuzzleSocketName.IsNone())
	{
		FMessageDialog::Debugf(NSLOCTEXT("Error", "Key", "MuzzleSoketName none initialize"));
		return FTransform::Identity;
	}
	return GetSkeletalMeshComponent()->GetSocketTransform(MuzzleSocketName);

}

void ABaseWeapon::BeginPlay()
{
	CurrentClip = ClipSize;
	MaxAmmo = TotalAmmo + ClipSize;
	Super::BeginPlay();

	if (GetOwner())
	{
		WeaponOwner = Cast<ACharacter>(GetOwner());
		if (!WeaponOwner)
		{
			UE_LOG(LogTemp, Warning, TEXT("Owner in not character"));
		}
	}
	OnAmmoChanged.Broadcast();
	GetMuzzleTransform();
}


void ABaseWeapon::MontageAnimPlay(ACharacter* OwnerChar, UAnimMontage* Montage, UAnimationAsset* Anim)
{
	if (Montage)
	{
		if (OwnerChar)
		{
			OwnerChar->PlayAnimMontage(Montage);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("No Owner found"));
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No montage"));
	}
	if (Anim)
	{
		GetSkeletalMeshComponent()->PlayAnimation(Anim, false);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No anim"));
	}
}
