// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyTestFolder/BasePickup.h"
#include "Health_Pickup.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT_API AHealth_Pickup : public ABasePickup
{
	GENERATED_BODY()
	
public:

	virtual	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health")
		float HealthPointsToAdd = 15;
};
