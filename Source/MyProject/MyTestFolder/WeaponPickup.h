// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyTestFolder/BasePickup.h"
#include "BaseWeapon.h"
#include "GameFramework/Actor.h"
#include "WeaponPickup.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT_API AWeaponPickup : public ABasePickup
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
		TSubclassOf<class ABaseWeapon> PickupWeapon_Class;

	virtual	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	
};
