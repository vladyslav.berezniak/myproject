// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponPickup.h"
#include "BaseWeapon.h"
#include "MyCharacter.h"


void AWeaponPickup::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

	AMyCharacter* MyChar = Cast<AMyCharacter>(OtherActor);
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherActor == MyChar) && (OtherComp != nullptr))
	{
		if (MyChar->WeaponToSpawn_Class != PickupWeapon_Class)
		{
			MyChar->WeaponToSpawn_Class = PickupWeapon_Class;
			MyChar->Drop();
			MyChar->CurrentWeapon = MyChar->SpawnWeapon();
			MyChar->AttachWeapon(MyChar->WeaponSocket);
		}
		else
		{
			MyChar->CurrentWeapon->CurrentClip = MyChar->CurrentWeapon->ClipSize;
			MyChar->CurrentWeapon->TotalAmmo = MyChar->CurrentWeapon->MaxAmmo;
			MyChar->CurrentWeapon->OnAmmoChanged.Broadcast();
		}
		Super::OnOverlapBegin(OverlappedComp, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
	}
}
