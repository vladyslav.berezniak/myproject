// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyTestFolder/BaseWeapon.h"
#include "Weapon_Projectile.generated.h"

/**
 * 
 */
UCLASS(abstract)
class MYPROJECT_API AWeapon_Projectile : public ABaseWeapon
{
	GENERATED_BODY()
	
protected:
	virtual void Fire() override;

		UPROPERTY(EditDefaultsOnly)
		TSubclassOf<class ABaseProjectile1> ProjectileClass;

};
