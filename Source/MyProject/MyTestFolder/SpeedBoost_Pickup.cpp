// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedBoost_Pickup.h"
#include <GameFramework/Character.h>
#include <GameFramework/CharacterMovementComponent.h>
#include <TimerManager.h>
#include "MyCharacter.h"

void ASpeedBoost_Pickup::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AMyCharacter* MyChar = Cast<AMyCharacter>(OtherActor);
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherActor == MyChar) && (OtherComp != nullptr))
	{
		FTimerDelegate Boost_TD;
		Boost_TD.BindUFunction(this, FName("SetSpeed"), UnBoostedSpeed, MyChar);
		SetSpeed(BoostedSpeed, MyChar);
		GetWorld()->GetTimerManager().SetTimer(BoostTime_TH, Boost_TD, BoostTime, false);
		Super::OnOverlapBegin(OverlappedComp, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
	}
}

void ASpeedBoost_Pickup::SetSpeed(float NewSpeed, AMyCharacter* BoostChar)
{
	BoostChar->SpeedBoost(NewSpeed);
	GetWorld()->GetTimerManager().ClearTimer(BoostTime_TH);
}

