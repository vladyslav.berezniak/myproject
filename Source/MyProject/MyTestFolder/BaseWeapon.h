// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/SkeletalMeshActor.h"
#include <DelegateCombinations.h>
#include "VitalityComponent.h"
#include "BaseWeapon.generated.h"

/**
 *
 */

UENUM(BlueprintType)
enum class EWeaponTypeEnum : uint8
{
	DE_Primary 			UMETA(DisplayName = "Primary"),
	DE_Secondary 		UMETA(DisplayName = "Secondary"),
	DE_Additional		UMETA(DisplayName = "Additional")
};

class UAnimMontage;
class ACharacter;
class UAnimationAsset;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FMyNoParamDelegate);

UCLASS(abstract)
class MYPROJECT_API ABaseWeapon : public ASkeletalMeshActor
{
	GENERATED_BODY()

public:

	ABaseWeapon();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "WeaponType")
		EWeaponTypeEnum WeaponEnumType;
	
	virtual void StartFire();

	virtual void StopFire();

	virtual void Reload();

	UPROPERTY(EditDefaultsOnly, Category = "Montages")
		UAnimMontage* FireMontage;
	UPROPERTY(EditDefaultsOnly, Category = "Montages")
		UAnimMontage* ReloadMontage;
	UPROPERTY(EditDefaultsOnly, Category = "Animation")
		UAnimationAsset* FireAnim;
	UPROPERTY(EditDefaultsOnly, Category = "Animation")
		UAnimationAsset* ReloadAnim;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UI")
		 class UTexture2D* WeaponIcon;

	void NewHit(FHitResult Hit);

	UPROPERTY(BlueprintAssignable, Category = "Ammo")
		FMyNoParamDelegate OnAmmoChanged;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Ammo")
		int32 TotalAmmo;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Ammo")
		int32 CurrentClip;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Ammo")
		int32 ClipSize;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Ammo")
		int32 MaxAmmo;

protected:


	UPROPERTY(EditDefaultsOnly, Category = "Muzzle")
		FName MuzzleSocketName;

	//Shots per minute
	UPROPERTY(EditDefaultsOnly, Category = "AutoRate", meta = (EditCondition = "bAutoFire"))
		float FireRate = 250;

	UPROPERTY(EditDefaultsOnly, Category = "AutoRate")
		bool bAutoFire = false;
	UPROPERTY(EditDefaultsOnly, Category = "FireDelay")
		bool bIsChargable = false;
	UPROPERTY(EditDefaultsOnly, Category = "FireDelay", meta = (EditCondition = "bIsChargable"))
		float ChargeTime = 0.0f;
	UPROPERTY(EditDefaultsOnly, Category = "FireDelay", meta = (EditCondition = "bIsChargable"))
	class UParticleSystem* ChargeEmitter;

	UPROPERTY(VisibleAnywhere)
	class UImpactEffectComponent* ImpactEffectComp;

	FTimerHandle AutoFite_TH;

	UPROPERTY()
		class ACharacter* WeaponOwner;

	UFUNCTION(BlueprintCallable)
		virtual void Fire();

	UFUNCTION()
		FTransform GetMuzzleTransform();

	
	//FHitResult WeaponHit;

	
	//UPROPERTY(VisibleAnywhere)

	virtual void BeginPlay() override;

	//void WeaponHit(ACharacter* Owner);

	
	void	MontageAnimPlay(ACharacter* OwnerChar, UAnimMontage* Montage, UAnimationAsset* Anim);


};
