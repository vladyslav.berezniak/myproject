// Fill out your copyright notice in the Description page of Project Settings.


#include "Health_Pickup.h"
#include "MyCharacter.h"
#include "VitalityComponent.h"

void AHealth_Pickup::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AMyCharacter* MyChar = Cast<AMyCharacter>(OtherActor);
	//MyChar->GetComponentByClass()
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherActor == MyChar) && (OtherComp != nullptr))
	{
		UVitalityComponent* VitComp = Cast<UVitalityComponent>(OtherActor->GetComponentByClass(UVitalityComponent::StaticClass()));
		if (VitComp)
		{
			VitComp->AddHealth(HealthPointsToAdd);
		}
		Super::OnOverlapBegin(OverlappedComp, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
	}


}

