// Fill out your copyright notice in the Description page of Project Settings.


#include "BasePickup.h"
#include <Components/BoxComponent.h>
#include <PhysXInterfaceWrapperCore.h>
#include <Kismet/GameplayStatics.h>
#include <Sound/SoundBase.h>
#include <Particles/ParticleSystem.h>
#include "MyCharacter.h"
#include "Runtime/Engine/Classes/Components/SceneComponent.h"
#include "Runtime/Engine/Classes/GameFramework/RotatingMovementComponent.h"

// Sets default values
ABasePickup::ABasePickup()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SceneComp = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComp;

	PickupMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PickupMesh"));
	PickupMesh->SetupAttachment(RootComponent);
	PickupMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	HitBox = CreateDefaultSubobject<UBoxComponent>(TEXT("HitBox"));
	HitBox->SetupAttachment(PickupMesh);
	HitBox->GetGenerateOverlapEvents();
	HitBox->OnComponentBeginOverlap.AddDynamic(this, &ABasePickup::OnOverlapBegin);

	RotationMove = CreateDefaultSubobject<URotatingMovementComponent>("RotatingMovement");
	RotationMove->RotationRate = FRotator(0.0, 90.0, 0.0);
}

// Called when the game starts or when spawned
void ABasePickup::BeginPlay()
{
	Super::BeginPlay();
}

void ABasePickup::SpawnEffects()
{
	if (PickupSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, PickupSound, GetActorLocation());
	}
	if (PickupParticle)
	{
		UGameplayStatics::SpawnEmitterAtLocation(this, PickupParticle, GetActorLocation());
	}
}

void ABasePickup::Respawn()
{
	FVector Location = GetActorLocation();
	FTransform ActorLocation = GetTransform();
	
}

void ABasePickup::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AMyCharacter* MyChar = Cast<AMyCharacter>(OtherActor);
	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherActor == MyChar) &&(OtherComp != nullptr))
	{
		SpawnEffects();
		PickupMesh->SetVisibility(false);
		SetLifeSpan(BoostTime + 1);
		if (bIsRespawnable)
		{
			GetWorld()->GetTimerManager().SetTimer(Respawn_TH, this, &ABasePickup::Respawn, RespawTime, false);
		}
	}
	HitBox->SetGenerateOverlapEvents(false);
}

// Called every frame
void ABasePickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

