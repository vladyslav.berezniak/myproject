// Fill out your copyright notice in the Description page of Project Settings.


#include "MyGameInstance.h"

int UMyGameInstance::GetCurrentScore()
{
	return CurrentScore;
}

void UMyGameInstance::AddScore(int ScoreToAdd)
{
	CurrentScore += ScoreToAdd;
}

void UMyGameInstance::FlushCurrentCsore()
{
	CurrentScore = 0;
}
