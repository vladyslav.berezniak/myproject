// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseProjectile1.h"
#include <GameFramework/ProjectileMovementComponent.h>
#include <Components/StaticMeshComponent.h>
#include <Particles/ParticleSystemComponent.h>
#include <Components/AudioComponent.h>
#include <Kismet/GameplayStatics.h>
#include "ImpactEffectActor.h"
#include <Engine/World.h>

// Sets default values
ABaseProjectile1::ABaseProjectile1()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MovementComp = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));

	Bullet = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet"));

}

// Called when the game starts or when spawned
void ABaseProjectile1::BeginPlay()
{
	Super::BeginPlay();

	

}

// Called every frame
void ABaseProjectile1::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABaseProjectile1::NotifyHit(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::NotifyHit(MyComp, Other, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalImpulse, Hit);

	if (OnHitFire.IsBound())
	{
		OnHitFire.Execute(Hit);
	}

	Destroy();
}

