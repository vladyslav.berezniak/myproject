// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ImpactEffectActor.generated.h"

UENUM(BlueprintType)		
enum class EDamageEnum : uint8
{
	DE_AnyDamage 			UMETA(DisplayName = "AnyDamage"),
	DE_RadialDamage 		UMETA(DisplayName = "RadialDamage"),
	DE_RadialDamageFalloff	UMETA(DisplayName = "RadialDamageFalloff")
};

UCLASS()
class MYPROJECT_API AImpactEffectActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AImpactEffectActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
		EDamageEnum DamageEnumType;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
		class UMaterialInterface* DecalMaterial;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
		class USoundBase* EffectSound;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
		class UParticleSystem* EffectParticle;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
		bool bApplyImpulse = true;

	UPROPERTY(EditDefaultsOnly, Category = "Effects", meta = (EditCondition = "bApplyImpulse"))
		float ImpulseStrength = 1000;

	UPROPERTY(EditDefaultsOnly, Category = "Damage", meta = (EditCondition = "DE_RadialDamage"))
		float DamageRadius = 75;

	UPROPERTY(EditDefaultsOnly, Category = "Damage", meta = (EditCondition = "DE_RadialDamageFalloff"))
		float MinDamage = 5;

	UPROPERTY(EditDefaultsOnly, Category = "Damage", meta = (EditCondition = "DE_RadialDamageFalloff"))
		float DamageInnerRadius = 5;

	UPROPERTY(EditDefaultsOnly, Category = "Damage", meta = (EditCondition = "DE_RadialDamageFalloff"))
		float DamageOuterRadius = 20;

	UPROPERTY(EditDefaultsOnly, Category = "Damage", meta = (EditCondition = "DE_RadialDamageFalloff"))
		float DamageFallof = 5;

	void SpawnEffects();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Damage")
		float Damage = 50;

	UFUNCTION()
		void DoDamage();

	FHitResult EffectHit;

public:	
	// Called every frame
	void HitInit(FHitResult Hit);
	virtual void Tick(float DeltaTime) override;

};
