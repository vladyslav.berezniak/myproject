// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyTestFolder/BaseWeapon.h"
#include "WeaponTrace.generated.h"

/**
 * 
 */
UCLASS(abstract)
class MYPROJECT_API AWeaponTrace : public ABaseWeapon
{
	GENERATED_BODY()
	virtual	void Fire() override;

	UFUNCTION()
		void SpawnBeam();

	UPROPERTY(EditDefaultsOnly, Category = "Trace")
	float TraceLength = 200;
	
	UPROPERTY(EditDefaultsOnly, Category = "Trace")
	float SphereRadius = 50;
	
	UPROPERTY()
	class UParticleSystemComponent* BeamComp;
	
	UPROPERTY()
	TArray<FHitResult> Hits;
	
	UPROPERTY()
	FVector TraceStart;
	
	UPROPERTY()
	FVector TraceEnd;
	
	UPROPERTY(EditDefaultsOnly)
	class UParticleSystem* Beam;

	virtual void Tick(float DeltaTime) override;
};
