// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MyCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FFuckingOneParamDelegate, ABaseWeapon*, Weapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FRollOneParamDelegate, bool, StartRoll);
class UCameraComponent;
class USpringArmComponent;
class UAnimMontage;
class USceneComponent;
class ABaseWeapon;

UCLASS()
class MYPROJECT_API AMyCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMyCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class	UVitalityComponent* VitalityComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UCameraComponent* CameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USpringArmComponent* SpringArmComponent;


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USceneComponent* WeaponComponent;


	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character Movement: Walking")
	float DefaultWalkSpeed = 600.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character Movement: Walking")
	float SprintSpeed = 1500.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Character Movement: Walking")
	bool bIsWeaponEquiped;

	UPROPERTY(EditAnywhere, Category = "Camera")
	float CameraMultiplier;

	UPROPERTY(EditAnywhere, Category = "Camera")
	float MaxCameraHeight;

	UPROPERTY(EditAnywhere, Category = "Camera")
	float MinCameraHeight;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bIsWalking = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	bool bCanFire = true;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	bool bIsSprinting = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		bool bIsRolling = false;

	FTimerHandle RollingTime_TH;

	UFUNCTION(BlueprintCallable)
	void CameraZoom(float Scale);
	
	void MoveForward(float Scale);
	void MoveRight(float Scale);
	void Sprint();
	void StopSprint();
	void StartWalk();
	void StopWalk();
	void OnJump();
	void Fire();
	void Reload();
	
	void StartFire();
	void StopFire();

	void StartRoll();
	void StopRoll();

	UFUNCTION()
	void CharacterDied();

public:
	// Called every frame
	UPROPERTY(BlueprintAssignable, Category = "Weapon")
	FFuckingOneParamDelegate OnWeaponChanged;
	UPROPERTY(BlueprintAssignable, Category = "Movement")
	FRollOneParamDelegate StartRollDelagate;

	

	virtual void Tick(float DeltaTime) override;
	UPROPERTY(EditDefaultsOnly, Category = "BaseWeapon")
		FName WeaponSocket;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UAnimMontage* EquipCurrentWeapon;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	UAnimMontage* UnEquipCurrentWeapon;
	
	UFUNCTION()
	ABaseWeapon* SpawnWeapon();
	
		void AttachWeapon(FName SocketName);
		void AttachWeapon();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		TSubclassOf<class ABaseWeapon> WeaponToSpawn_Class;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class ABaseWeapon* CurrentWeapon;
	void Drop();
	UFUNCTION(BlueprintCallable)
	void SpeedBoost(float NewSpeed);
	
	void RotateToMouse();


	UFUNCTION(BlueprintCallable)
	void EquipWeapon();
	UFUNCTION(BlueprintCallable)
	void UnEquipWeapon();
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
