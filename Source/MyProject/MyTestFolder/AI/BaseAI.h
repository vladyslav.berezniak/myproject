// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BaseAI.generated.h"

UCLASS()
class MYPROJECT_API ABaseAI : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABaseAI();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/*UFUNCTION(BlueprintCallable)
	virtual TArray<AActor*>& OnActorsDetected(TArray<AActor*>& DetectedActors);*/

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class	UVitalityComponent* VitalityComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class	UWidgetComponent* VitalityWidget;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class	UAIPerceptionComponent* AIPerceptionComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class	UAISenseConfig_Sight* SightConfig;
	/*UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UPawnSensingComponent* AI_SensingComp;*/
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float	AISightRadius = 800.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float	AILoseSightRadius = 1000.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float	AISightAge = 8.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float	AIPeripheralSightAngle = 90.0f;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	//virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
