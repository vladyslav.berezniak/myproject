// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseAI.h"
#include "../VitalityComponent.h"
#include <Widget.h>
#include "Components/WidgetComponent.h"
#include <UserWidget.h>
#include <ConstructorHelpers.h>
#include "Runtime/AIModule/Classes/AIController.h"
#include "Runtime/AIModule/Classes/Perception/AIPerceptionComponent.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Runtime/AIModule/Classes/AIController.h"



// Sets default values
ABaseAI::ABaseAI()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	VitalityComp = CreateDefaultSubobject<UVitalityComponent>(TEXT("VitalityComponent"));
	
	VitalityWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("VitalityWidget"));
	VitalityWidget->SetupAttachment(RootComponent);
	
	/*static ConstructorHelpers::FClassFinder<UUserWidget>AI_VitalityWidget(TEXT("WidgetBlueprint'/Game/MyAssets/UI/WBP_Vitality.WBP_Vitality_C'"));
	if (AI_VitalityWidget.Succeeded())
	{
		VitalityWidget->SetWidgetClass(AI_VitalityWidget.Class);
	}*/ 
	
	VitalityWidget->AddLocalTransform(FTransform(FRotator(90, 0, 0).Quaternion(), FVector(0, 0, -90), FVector(1.0, 0.5, 0.5)));
	AIPerceptionComp = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("AIPerception"));
	
	SightConfig = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("AISightConfig"));
	//SightConfig->DetectionByAffiliation.ShouldDetectAll();
	SightConfig->DetectionByAffiliation.bDetectEnemies = true;
	SightConfig->DetectionByAffiliation.bDetectFriendlies = true;
	SightConfig->DetectionByAffiliation.bDetectNeutrals = true;
	SightConfig->SightRadius = AISightRadius;
	SightConfig->LoseSightRadius = AILoseSightRadius;
	SightConfig->PeripheralVisionAngleDegrees = AIPeripheralSightAngle;
	SightConfig->SetMaxAge(AISightAge);


	AIPerceptionComp->ConfigureSense(*SightConfig);
	AIPerceptionComp->SetDominantSense(SightConfig->GetSenseImplementation());
	//
	//AIPerceptionComp->OnPerceptionUpdated.AddDynamic(this, &ABaseAI::OnActorsDetected);
	
	//GetPerceptionComponent()->OnPerceptionUpdated.AddDynamic(this, &ABaseAI::OnActorsDetected);

	if (GetMesh())
	{
		GetMesh()->AddLocalTransform(FTransform(FRotator(0, -90, 0).Quaternion(), FVector(0, 0, -90)));
	}
}

// Called when the game starts or when spawned
void ABaseAI::BeginPlay()
{
	Super::BeginPlay();
	VitalityComp->OnHealthChanged.Broadcast(); 
	
}

//TArray<AActor*>& ABaseAI::OnActorsDetected(TArray<AActor*>& DetectedActors = )
//{
//	return DetectedActors;
//}

// Called every frame
void ABaseAI::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


// Called to bind functionality to input
//void ABaseAI::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
//{
//	Super::SetupPlayerInputComponent(PlayerInputComponent);
//
//}

