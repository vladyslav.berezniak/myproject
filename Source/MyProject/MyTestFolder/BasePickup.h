// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BasePickup.generated.h"

UCLASS(abstract)
class MYPROJECT_API ABasePickup : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABasePickup();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, Category = "Effects")
		class USoundBase* PickupSound;

	UPROPERTY(EditAnywhere, Category = "Effects")
		class UParticleSystem* PickupParticle;
	UPROPERTY(EditDefaultsOnly, Category = "Timer")
		float BoostTime = 5.0f;
	UPROPERTY(EditDefaultsOnly, Category = "Respawn")
		bool bIsRespawnable = false;
	UPROPERTY(EditDefaultsOnly, Category = "Respawn", meta = (EditCondition = "bIsRespawnable"))
		float RespawTime = 10.0f;
	UPROPERTY(EditDefaultsOnly, Category = "Respawn", meta = (EditCondition = "bIsRespawnable"))
		FTimerHandle Respawn_TH;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UStaticMeshComponent* PickupMesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class URotatingMovementComponent* RotationMove;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UBoxComponent* HitBox;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class USceneComponent* SceneComp;
	
	UFUNCTION(BlueprintCallable)
	virtual	void SpawnEffects();
	UFUNCTION(BlueprintCallable)
	virtual	void Respawn();


public:	
	UFUNCTION(BlueprintCallable)
	virtual	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
