// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "MyGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT_API UMyGameInstance : public UGameInstance
{
	GENERATED_BODY()

private:

	int CurrentScore = 0;

public:
	UFUNCTION(BlueprintPure)
	int GetCurrentScore();
	
	UFUNCTION(BlueprintCallable)
	void AddScore(int ScoreToAdd);
	
	UFUNCTION(BlueprintCallable)
	void FlushCurrentCsore();
	
};
