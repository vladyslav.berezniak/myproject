// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyTestFolder/BasePickup.h"
#include "SpeedBoost_Pickup.generated.h"

/**
 * 
 */
UCLASS()
class MYPROJECT_API ASpeedBoost_Pickup : public ABasePickup
{
	GENERATED_BODY()

public:

	virtual	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;
protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Speed")
		float BoostedSpeed = 900;
	UPROPERTY(EditDefaultsOnly, Category = "Timer")
		FTimerHandle BoostTime_TH;
	UPROPERTY()
		class AMyCharacter* OverlappedChar;
	UPROPERTY(BlueprintReadOnly)
		float UnBoostedSpeed = 600;
	UFUNCTION(BlueprintCallable)
		void SetSpeed(float NewSpeed, AMyCharacter* BoostChar);
};
