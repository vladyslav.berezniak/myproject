// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BasePickup.h"
#include <Map.h>
#include "VitalityComponent.generated.h"



DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNoParamDelegate);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class MYPROJECT_API UVitalityComponent : public UActorComponent
{
	GENERATED_BODY()

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Health")
		float MaxHealth = 100;
	UPROPERTY(BlueprintReadWrite, Category = "Health")
		float CurrentHealth;// = MaxHealth;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health")
		float RecoverStep = 1;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health")
		float RecoverStepTime = 2;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Health")
		float RecoverDelay = 3;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Score")
		int Score = 100;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Spawn")
		bool bSpawnOnDeath = true;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Spawn", meta = (EditCondition = "bSpawnOnDeath"))
		float SpawnChance = 1.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Spawn", meta = (EditCondition = "bSpawnOnDeath"))
		TMap<TSubclassOf<ABasePickup>, float> SpawnMap;

	//UPROPERTY(EditDefaultsOnly, Category = "Recover")
	FTimerHandle RecoverTime_TH;

	UFUNCTION()
	void RecoverDamage();

	bool ShouldSpawn(float Chance);
	void StartRecover();
	void StopRecover();

public:
	// Called every frame
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Health")
		bool bCanTakeDamage = true;
	UPROPERTY(BlueprintAssignable, Category = "Health")
		FNoParamDelegate OnHealthChanged;
	UPROPERTY(BlueprintAssignable, Category = "Health")
		FNoParamDelegate OnOwnerDied;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION()
		void AddHealth(float HealthToAdd);

	// Sets default values for this component's properties
	UVitalityComponent();

	UFUNCTION()
		void DamageHandle(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	UFUNCTION(BlueprintCallable)
		bool IsAlive();

	UFUNCTION(BlueprintCallable)
		void SpawnPickup(FVector Loc, FRotator Rot);

};
