// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponTrace.h"
#include <CollisionQueryParams.h>
#include <WorldCollision.h>
#include <Kismet/KismetSystemLibrary.h>
#include <Private/KismetTraceUtils.h>
#include "BaseWeapon.h"
#include "ImpactEffectComponent.h"
#include <GameFramework/Character.h>
#include <Kismet/GameplayStatics.h>
#include <Particles/ParticleSystemComponent.h>




void AWeaponTrace::Fire()
{
	if (!WeaponOwner->GetMesh()->GetAnimInstance()->Montage_IsPlaying(ReloadMontage))
	{
		if (CurrentClip > 0)
		{
			TraceStart = GetMuzzleTransform().GetLocation();
			TraceEnd = GetMuzzleTransform().GetRotation().GetForwardVector() * TraceLength + TraceStart;

			FCollisionObjectQueryParams CollisionParams;
			CollisionParams.AddObjectTypesToQuery(ECC_PhysicsBody);
			CollisionParams.AddObjectTypesToQuery(ECC_WorldDynamic);
			CollisionParams.AddObjectTypesToQuery(ECC_WorldStatic);

			FCollisionShape CollisionShape = FCollisionShape::MakeSphere(SphereRadius);
			GetWorld()->SweepMultiByObjectType(Hits, TraceStart, TraceEnd, FQuat::Identity, CollisionParams, CollisionShape);

#if ENABLE_DRAW_DEBUG

#endif


			SpawnBeam();
			if (Hits.Num() > 0)
			{
				for (auto TmpHit : Hits)
				{

					ImpactEffectComp->SpawnImpactEffect(TmpHit);
				}
			}
		}
	}
	Super::Fire();
}

void AWeaponTrace::SpawnBeam()
{
	if (Beam)
	{
		BeamComp = UGameplayStatics::SpawnEmitterAttached(Beam, RootComponent, MuzzleSocketName, FVector::ZeroVector, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, true);
	}
}

void AWeaponTrace::Tick(float DeltaTime)
{
	if (BeamComp)
	{
		BeamComp->SetBeamTargetPoint(0, TraceEnd, 0);
	}

}
