// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon_Projectile.h"
#include "BaseProjectile1.h"
#include <GameFramework/Character.h>
#include "ImpactEffectComponent.h"

void AWeapon_Projectile::Fire()
{
	//Spawn projectile
	if (!WeaponOwner->GetMesh()->GetAnimInstance()->Montage_IsPlaying(ReloadMontage))
	{
		if (ProjectileClass)
		{
			FTransform TmpTransform = GetMuzzleTransform();

			if (CurrentClip > 0)
			{
				ABaseProjectile1* TmpProj = Cast<ABaseProjectile1>(GetWorld()->SpawnActor(ProjectileClass, &TmpTransform));
				TmpProj->OnHitFire.BindDynamic(ImpactEffectComp, &UImpactEffectComponent::SpawnImpactEffect);
			}
		}
	}
	Super::Fire();

}

