// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MyTestFolder/BaseProjectile1.h"
#include <TimerManager.h>
#include <Engine/EngineTypes.h>
#include "AttachedProjectile.generated.h"

/**
 * 
 */

UCLASS()
class MYPROJECT_API AAttachedProjectile : public ABaseProjectile1
{
	GENERATED_BODY()
public:
	
	
	virtual void NotifyHit(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit) override;
	
protected:

	UPROPERTY(EditDefaultsOnly, Category = "Timer")
		FTimerHandle ExplosionTime_TH;
	UPROPERTY(EditDefaultsOnly, Category = "Timer")
		FHitResult ExpHit;

	void ExplosionPlay();
	
};
